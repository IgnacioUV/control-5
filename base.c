#include <stdio.h>
#include <stdlib.h>
#include "estudiantes.h"
#include <math.h>


float desvStd(estudiante curso[]){
	float prom=0,desv=0;
	for(int i=0;i<24;i++){
		prom=prom+curso[i].prom;
	}
	prom=prom/24;
	for(int i=0;i<24;i++){
		desv=desv+( pow ((curso[i].prom - prom),2));
	}
	desv=desv/24;
	desv= sqrt (desv);

	return desv;
}

float menor(estudiante curso[]){
	float menor=8.0;
	for(int i=0;i<24;i++){
		if(menor>curso[i].prom){
			menor=curso[i].prom;
		}
	}
	return menor;
}

float mayor(estudiante curso[]){
	float mayor=0.0;
	for(int i=0;i<24;i++){
		if(mayor<curso[i].prom){
			mayor=curso[i].prom;
		}
	}
	return mayor;
}

void registroCurso(estudiante curso[]){
	//debe registrar las calificaciones
	for(int i=0;i<24;i++){
		printf("Nombre: %s %s %s\n",curso[i].nombre,curso[i].apellidoP,curso[i].apellidoM);
		printf("Ingrese nota P1: "); scanf("%f",&curso[i].asig_1.proy1); printf("Ingrese nota P2: "); scanf("%f",&curso[i].asig_1.proy2); printf("Ingrese nota P3: "); scanf("%f",&curso[i].asig_1.proy3);
		printf("Ingrese nota C1: "); scanf("%f",&curso[i].asig_1.cont1); printf("Ingrese nota C2: "); scanf("%f",&curso[i].asig_1.cont2); printf("Ingrese nota C3: "); scanf("%f",&curso[i].asig_1.cont3); printf("Ingrese nota C4: "); scanf("%f",&curso[i].asig_1.cont4); printf("Ingrese nota C5: "); scanf("%f",&curso[i].asig_1.cont5); printf("Ingrese nota C6: "); scanf("%f",&curso[i].asig_1.cont6);
		curso[i].prom=(((curso[i].asig_1.proy1+curso[i].asig_1.proy2+curso[i].asig_1.proy3)/3)*0.7)+(((curso[i].asig_1.cont1+curso[i].asig_1.cont2+curso[i].asig_1.cont3+curso[i].asig_1.cont4+curso[i].asig_1.cont5+curso[i].asig_1.cont6)/6)*0.3);
	//se registra las notas de cada estudiante en la estructura
	}
	 

}

void clasificarEstudiantes(estudiante curso[]){
	FILE *file1,*file2;
	int temp,prom[]={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23};

	if(((file1=fopen("reprobados.txt","w"))==NULL)||((file2=fopen("aprovados.txt","w"))==NULL)){
		printf("\nError al crear archivo");
		exit(0);
	}
	else{
		for(int i=0; i<24;i++){
			
			for(int j=i;j<24;j++){
				if(curso[prom[i]].prom<curso[prom[j]].prom){
					temp=prom[i];
					prom[i]=prom[j];
					prom[j]=temp;

				}
			}
		}

		for(int i=0;i<24;i++){
			
			if(curso[prom[i]].prom>3.95){
				fprintf(file2,"%s\t%s\t%s\t%.1f\n", curso[prom[i]].nombre,curso[prom[i]].apellidoP, curso[prom[i]].apellidoM, curso[prom[i]].prom);
			}
			else{
				fprintf(file1,"%s\t%s\t%s\t%.1f\n", curso[prom[i]].nombre,curso[prom[i]].apellidoP, curso[prom[i]].apellidoM, curso[prom[i]].prom);
			}
		}
		fclose(file1);
		fclose(file2);
	} 
}


void metricasEstudiantes(estudiante curso[]){
	 float Mayor,Menor,desv;
	 Mayor=mayor(curso);
	 Menor=menor(curso);
	 desv=desvStd(curso);

	 printf("El mayor promedio es: %.1f\n",Mayor);
	 printf("El menor promedio es: %.1f\n",Menor);
	 printf("La desviacion Estandar es: %.1f\n",desv);

}





void menu(estudiante curso[]){
	int opcion;
    do{
		printf( "\n   1. Cargar Estudiantes" );
		printf( "\n   2. Ingresar notas" );
		printf( "\n   3. Mostrar Promedios" );
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes " );
		printf( "\n   6. Salir." );
		printf( "\n\n   Introduzca opción (1-6): " );
		scanf( "%d", &opcion );
        switch ( opcion ){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //carga en lote una lista en un arreglo de registros
					break;

			case 2: registroCurso(curso);// Realiza ingreso de notas en el registro curso 
					break;

			case 3: metricasEstudiantes(curso); //presenta métricas de disperción, tales como mejor promedio; más bajo; desviación estándard.
					break;

			case 4: grabarEstudiantes("test.txt",curso);
					break;
            case 5: clasificarEstudiantes(curso); // elimine el "destino" ya que no se para que era
            		break;
         }

    } while ( opcion != 6 );
}

int main(){
	estudiante curso[30]; 	
	menu(curso); 
	return EXIT_SUCCESS; 
}